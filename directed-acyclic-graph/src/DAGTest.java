import static org.junit.Assert.*;

import org.junit.Test;

public class DAGTest {

	@Test
	public void testNewDAG() {
		DAG dag = new DAG();
		assert(dag.root == null);
		assert(dag.nodes == null);
		assert(dag.relationships == null);
		assert(dag.size == 0);
	}

	/*@Test
	public void testShowRelationships() {
		DAG dag = new DAG();
		assertEquals("No relationships to show.", dag.showRelationships());
		
		dag.addNode(1,1);
		assertEquals("No relationships to show.", dag.showRelationships());
		
		dag.addNode(2,2);
		dag.addRelationship(1,2);
		assertEquals("1 -> 2", dag.showRelationships());
		
		dag.addNode(3,3);
		dag.addNode(0,0);
		dag.addRelationship(1,3);
		dag.addRelationship(2,0);
		assertEquals(	"1 -> 2\n"+
						"1 -> 3\n"+
						"2 -> 0\n", dag.showRelationships());
		

		dag.addRelationship(0,3);
		assertEquals(	"1 -> 2\n"+
						"1 -> 3\n"+
						"2 -> 0\n"+
						"0 -> 3", dag.showRelationships());
		
		dag.addRelationship(2,3);
		assertEquals(	"1 -> 2\n"+
						"1 -> 3\n"+
						"2 -> 0\n"+
						"0 -> 3\n"+
						"2 -> 3", dag.showRelationships());
	}*/

	@Test
	public void testShowNodes() {
		DAG dag = new DAG();
		assertEquals("No nodes to show.", dag.showNodes());
		
		assertTrue(dag.addNode(0,1));
		assertEquals("[0,1]\n", dag.showNodes());
		
		assertTrue(dag.addNode(1,1));
		assertTrue(dag.addNode(2,2));
		assertEquals("[0,1]\n"
					+ "[1,1]\n"
					+ "[2,2]\n", dag.showNodes());
	}

	@Test
	public void testAddNode() {
		DAG dag = new DAG();
		assertEquals("No nodes to show.", dag.showNodes());
		
		assertTrue(dag.addNode(0,0));
		assertEquals("[0,0]\n", dag.showNodes());
		
		assertTrue(dag.addNode(1,1));
		assertTrue(dag.addNode(2,2));
		assertEquals("[0,0]\n"
					+ "[1,1]\n"
					+ "[2,2]\n", dag.showNodes());
		
		//Add a node that's already part of the graph
		assertFalse(dag.addNode(2,2));
		assertEquals("[0,0]\n"
					+ "[1,1]\n"
					+ "[2,2]\n", dag.showNodes());
		
		assertEquals("No relationships to show.", dag.showRelationships());
	}

	@Test
	public void testAddRelationship() {
		DAG dag = new DAG();
		
		//Add nodes to graph
		dag.addNode(0,0);
		dag.addNode(1,1);
		dag.addNode(2,2);
		dag.addNode(3,3);
		dag.addNode(4,4);
		dag.addNode(5,5);
		dag.addNode(6,6);
		
		//Test relationship adding
		assertEquals("No relationships to show.", dag.showRelationships());	

		/*	0
		 * 	|
		 * 	3
		 */
		dag.addRelationship(0,3);
		assertEquals(	"0 -> 3\n", dag.showRelationships());
		
		//Adding relationships that would create a cycle in the graph
		dag.addRelationship(3,0);
		assertEquals(	"0 -> 3\n", dag.showRelationships());/* 1 nonexistent node */
		
		//Adding relationships to nodes that don't exist in this graph
		/* 1 nonexistent node */
		dag.addRelationship(0,9);
		assertEquals(	"0 -> 3\n", dag.showRelationships());
		dag.addRelationship(9,0);
		assertEquals(	"0 -> 3\n", dag.showRelationships());
		
		/* 2 nonexistent nodes */
		dag.addRelationship(8,9);
		assertEquals(	"0 -> 3\n", dag.showRelationships());

		//Adding relationships to existent nodes
		/*	0
		 * 	|\
		 * 	3 4
		 */
		dag.addRelationship(0,4);
		assertEquals(	"0 -> 3"+
						"0 -> 4\n", dag.showRelationships());
		
		/*	0
		 * 	|\
		 * 	3 4 
		 * 	|/|
		 * 	1 6
		 */
		dag.addRelationship(3,1);
		dag.addRelationship(4,1);
		dag.addRelationship(4,6);
		assertEquals(	"0 -> 3"+
						"0 -> 4\n"+
						"3 -> 1\n"+
						"4 -> 1\n"+
						"4 -> 6\n", dag.showRelationships());
		
		/*	0
		 * 	|\
		 * 	3 4 
		 * 	|/|\
		 * 	1 6 |
		 *   \| |
		 *    2 |
		 *    |/
		 *    5
		 */
		dag.addRelationship(6,2);
		dag.addRelationship(1,2);
		dag.addRelationship(2,5);
		dag.addRelationship(4,5);
		assertEquals(	"0 -> 3"+
						"0 -> 4\n"+
						"3 -> 1\n"+
						"4 -> 1\n"+
						"4 -> 6\n"+
						"1 -> 2\n"+
						"6 -> 2\n"+
						"2 -> 5\n"+
						"4 -> 5\n", dag.showRelationships());
		
		
		
	}

	/*
	@Test
	public void testRemoveNode() {
		//TODO: is this needed?
	}

	@Test
	public void testRemoveRelationship() {
		//TODO: is this needed?
	}

	@Test
	public void testCreatesCycle() {
		//TODO: is this needed?
	}

	@Test
	public void testIsConnected() {
		//TODO: is this needed?
	} 
	*/

	@Test
	public void testLCA() {
		DAG dag = new DAG();
		
		//Add nodes to graph
		dag.addNode(0,0);
		dag.addNode(1,1);
		dag.addNode(2,2);
		dag.addNode(3,3);
		dag.addNode(4,4);
		dag.addNode(5,5);
		dag.addNode(6,6);
		
		//Add relationships to graph
		dag.addRelationship(0,3);
		dag.addRelationship(0,4);
		dag.addRelationship(3,1);
		dag.addRelationship(4,1);
		dag.addRelationship(4,6);
		dag.addRelationship(1,2);
		dag.addRelationship(6,2);
		dag.addRelationship(2,5);
		dag.addRelationship(4,5);
		assertEquals(	"0 -> 3\n"+
						"0 -> 4\n"+
						"3 -> 1\n"+
						"4 -> 1\n"+
						"4 -> 6\n"+
						"1 -> 2\n"+
						"6 -> 2\n"+
						"2 -> 5\n"+
						"4 -> 5\n", dag.showRelationships());

		/*	0
		 * 	|\
		 * 	3 4 
		 * 	|/|\
		 * 	1 6 |
		 *   \| |
		 *    2 |
		 *    |/
		 *    5
		 */
		assertTrue(dag.LCA(3,4).equals(0));
		assertTrue(dag.LCA(3,6).equals(0));
		assertTrue(dag.LCA(1,6).equals(4));
		assertTrue(dag.LCA(2,1).equals(1));
		assertTrue(dag.LCA(2,5).equals(2));
		
		//Non-existent nodes
		assertTrue(dag.LCA(3,9).equals(null));
		assertTrue(dag.LCA(9,1).equals(null));
		assertTrue(dag.LCA(8,9).equals(null));
	}

}
