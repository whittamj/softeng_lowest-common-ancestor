import java.util.ArrayList;

public class DAG<Key extends Comparable<Key>, Value> {
//ATTRIBUTES
	public Node root;
	public int size;
	ArrayList<Node> nodes;
	ArrayList<Relationship> relationships;
	
	private static String NO_NODES_STR = "No nodes to show.";
	private static String NO_RELATIONSHIPS_STR = "No relationships to show."; 
	
//SUBCLASSES
	private class Node {
		public Key key;           // sorted by key
	    public Value val;         // associated data
	    
	    public Node(Key k, Value v) {
	    	this.key = k;
	    	this.val = v;
	    }
	}
	
	private class Relationship {
		// Direction of relationship: k0 -> k1
		public Key k0, k1; 
		
		public Relationship(Key k0, Key k1) {
			this.k0 = k0;
			this.k1 = k1;
		}
	}
	
//CONSTRUCTOR
	public DAG() {
		root = null;
		nodes = null;
		relationships = null;
		size = 0;
	}
	
//METHODS
	public boolean addNode(Key key, Value val) {
		boolean nodeExists = false;
		
		if (nodes==null) nodes = new ArrayList<Node>();
		
		Node currentNode;
		for (int i=0; i<nodes.size() && !nodeExists; i++) {
			currentNode = nodes.get(i);
			if (currentNode.key==key && currentNode.val==val)
				nodeExists = true;
		}
		if (!nodeExists) {
			Node newNode = new Node(key, val);
			nodes.add(newNode);
			
			if(root==null) root = newNode;
		}
		return !nodeExists;
	}

	public boolean addRelationship(Key k0, Key k1) {
		if (relationships==null) relationships = new ArrayList<Relationship>();
		
		boolean alreadyExists = false;
		Relationship thisRel;
		for (int i=0; i<relationships.size() && !alreadyExists; i++) {
			thisRel = relationships.get(i);
			if (thisRel.k0.equals(k0) && thisRel.k1.equals(k1))
				alreadyExists = true;
		}
		if (!alreadyExists && !createsCycle(k0,k1)) //TODO WARNING! createsCycle() hasn't been implemented properly
			relationships.add(new Relationship(k0,k1));
		return !alreadyExists;
	}
	
	private boolean createsCycle(Key k0, Key k1) {
		//TODO implement correctly
		return false;
	}

	public String showRelationships() {
		if (relationships == null)
			return NO_RELATIONSHIPS_STR;
		
		String result = "";
		Relationship currentRel;
		for (int i=0; i<relationships.size(); i++) {
			currentRel = relationships.get(i);
			result += currentRel.k0 + " -> " 
						+ currentRel.k1 + "\n";
		}
		return result;
	}
	
	public String showNodes() {
		if (nodes == null)
			return NO_NODES_STR;
		
		String result = "";
		Node currentNode;
		for (int i=0; i<nodes.size(); i++) {
			currentNode = nodes.get(i);
			result += "[" + currentNode.key + "," 
						+ currentNode.val + "]\n";
		}
		return result;
	}
	
	public Key LCA(Key k0, Key k1) {
		if (root==null) return null;
		if (k0.equals(k1)) return k0;
		if (k0.equals(root.key) || k1.equals(root.key)) return root.key;
		
		ArrayList<Key> path0, path1;
		Key LCA, currentAncestor;
		int LCADistance, currentDistance;
		LCA = null;
		LCADistance = -1;
		
		System.out.println("k0: "+k0+"\nk1: "+k1);
		for (int i=0; i<nodes.size(); i++) {
			currentAncestor = nodes.get(i).key;
			System.out.println("currentAncestor: "+currentAncestor);
			path0 = getShortestPath(currentAncestor, k0);
			path1 = getShortestPath(currentAncestor, k1);
			
			if (path0!=null && path1!=null) {
				currentDistance = path0.size() + path1.size() -2;
				if (LCA==null || currentDistance<LCADistance) {
					LCA = currentAncestor;
					LCADistance = currentDistance;
				}
			}
		}
		return LCA;
	}
	
	private ArrayList<Key> getShortestPath(Key rootKey, Key leafKey) {
		ArrayList<ArrayList<Key>> allPaths = getPaths(rootKey, leafKey);
		if (allPaths==null) return null;
		System.out.println("all: "+allPaths.toString());
		System.out.println("0: "+allPaths.get(0).toString());
		if (allPaths.get(0)==null) return null;
		
		ArrayList<Key> shortestPath, currentPath;
		int shortestDistance, currentDistance;
		shortestPath = allPaths.get(0);
		shortestDistance = -1;
		
		for (int i=0; i<allPaths.size(); i++) {
			currentPath = allPaths.get(i);
			currentDistance = currentPath.size();
			if (currentDistance < shortestDistance) {
				shortestPath = currentPath;
				shortestDistance = currentDistance;
			}
		}
		return shortestPath;
	}
	
	private ArrayList<ArrayList<Key>> getPaths(Key rootKey, Key key) {
		if (nodes == null) return null;
		if (relationships == null && !rootKey.equals(key)) return null;
		
		ArrayList<ArrayList<Key>> paths = new ArrayList<ArrayList<Key>>();
		getPaths(paths, null, key, rootKey);
		return paths;
	}
	//TODO should AllPaths be returned?
	private void getPaths(ArrayList<ArrayList<Key>> allpaths, ArrayList<Key> currentPath, Key key, Key rootKey) {
		Relationship thisRel;
		boolean keyFound = false;
		
		for (int i=0; i<relationships.size() && !keyFound; i++) {
			thisRel = relationships.get(i);
			if (thisRel.k0.equals(rootKey)) {
				if (currentPath==null) {
					currentPath = new ArrayList<Key>();
					currentPath.add(rootKey);
				}
				currentPath.add(thisRel.k1);
				
				/* Case 1: found key */
				if (thisRel.k1.equals(key)) {
					allpaths.add(currentPath);
					keyFound = true;
				}
				/* Case 2: continue recursing */
				else {
					getPaths(allpaths, currentPath, key, thisRel.k1);
				}
			}
		}
	}
	
}
