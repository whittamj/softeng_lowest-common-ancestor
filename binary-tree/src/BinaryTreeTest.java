import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class BinaryTreeTest {
	
	@Test
	public void testContains(){
		BinaryTree<Integer, Integer> bt = new BinaryTree<Integer, Integer>();
		assertFalse(bt.contains(0));
		
		bt.put(0, 0);
		assertTrue(bt.contains(0));
		assertFalse(bt.contains(1));
		
		bt.put(1, 1);
		bt.put(2, 2);
		bt.put(3, 3);
		bt.put(4, 4);
		assertTrue(bt.contains(2));
		assertFalse(bt.contains(7));
	}

	@Test
	public void testGet(){
		BinaryTree<Integer, Integer> bt = new BinaryTree<Integer, Integer>();
		//cannot use assertEquals() to compare Value objects
		assertEquals(null, bt.get(0));
				
		bt.put(7, 7);
		bt.put(0, 0);
		
		assertTrue(bt.get(0).equals(0)); 
		assertEquals(null, bt.get(5)); 
		
		bt.put(1, 1);
		bt.put(2, 2);
		bt.put(3, 3);
		bt.put(4, 4);
		assertTrue(bt.get(4).equals(4));
		assertTrue(bt.get(7).equals(7)); 
		assertEquals(null, bt.get(6));  
	}

	@Test
	public void testPut(){
		BinaryTree<Integer, Integer> bt = new BinaryTree<Integer, Integer>();
		assertEquals("()", bt.printKeysInOrder());
		
		bt.put(9, null);
		bt.put(4, 4);
		assertEquals("(()4())", bt.printKeysInOrder());
		
		bt.put(4, 4);
		assertEquals("(()4())", bt.printKeysInOrder());
		
		bt.put(6, 6);
		bt.put(7, 7);
		bt.put(5, 5);
		assertEquals("(()4((()5())6(()7())))", bt.printKeysInOrder());
		
		bt.put(0, 0);
		bt.put(2, 2);
		bt.put(1, 1);
		assertEquals("((()0((()1())2()))4((()5())6(()7())))", bt.printKeysInOrder());
	}

	@Test
    public void testDelete() {
        BinaryTree<Integer, Integer> bt = new BinaryTree<Integer, Integer>();
        bt.delete(1);
        assertEquals("Deleting from empty tree", "()", bt.printKeysInOrder());
        
        bt.put(9, 9);
        bt.delete(9);
        assertEquals("Deleting the only element in a tree", "()", bt.printKeysInOrder());
        
        bt.put(9, 9);
        bt.put(10, 10);
        bt.delete(9);
        assertEquals("Deleting node with a null left branch", "(()10())", bt.printKeysInOrder());
        bt.delete(10);
        
        bt.put(7, 7);   //        _7_
        bt.put(8, 8);   //      /     \
        bt.put(3, 3);   //    _3_      8
        bt.put(1, 1);   //  /     \
        bt.put(2, 2);   // 1       6
        bt.put(6, 6);   //  \     /
        bt.put(4, 4);   //   2   4
        bt.put(5, 5);   //        \
                         //         5
        
        assertEquals("Confirming order of constructed tree",
                "(((()1(()2()))3((()4(()5()))6()))7(()8()))", bt.printKeysInOrder());
        
        bt.delete(9);
        assertEquals("Deleting non-existent key",
                "(((()1(()2()))3((()4(()5()))6()))7(()8()))", bt.printKeysInOrder());

        bt.delete(8);
        assertEquals("Deleting leaf", "(((()1(()2()))3((()4(()5()))6()))7())", bt.printKeysInOrder());

        bt.delete(6);
        assertEquals("Deleting node with single child",
                "(((()1(()2()))3(()4(()5())))7())", bt.printKeysInOrder());

        bt.delete(3);
        assertEquals("Deleting node with two children",
                "(((()1())2(()4(()5())))7())", bt.printKeysInOrder());
    }

	@Test
	public void testGetMax(){
		BinaryTree<Integer, Integer> bt = new BinaryTree<Integer, Integer>();
		assertEquals(null, bt.getMax());
		
		bt.put(4, 4);
		assertTrue(bt.getMax().key.equals(4));
		bt.delete(4);
		assertEquals(null, bt.getMax());
		
		bt.put(7, 7);
		bt.put(8, 8);
		assertTrue(bt.getMax().key.equals(8));
		
		bt.put(6, 6);
		bt.put(5, 5);
		bt.put(0, 0);
		bt.put(2, 2);
		bt.put(1, 1);
		assertTrue(bt.getMax().key.equals(8));
	}

	@Test
	public void testSize(){
		BinaryTree<Integer, Integer> bt = new BinaryTree<Integer, Integer>();
		assertEquals(0, bt.size());
		
		bt.put(0, 0);
		assertEquals(1, bt.size());
		
		bt.put(1, 1);
		bt.put(2, 2);
		bt.put(3, 3);
		bt.put(4, 4);
		assertEquals(5, bt.size());
	}

	@Test
	public void testPrintKeysInOrder(){
		BinaryTree<Integer, Integer> bt = new BinaryTree<Integer, Integer>();
		assertEquals("()", bt.printKeysInOrder());
		
		bt.put(4, 4);
		assertEquals("(()4())", bt.printKeysInOrder());
		
		bt.put(7, 7);
		bt.put(8, 8);
		bt.put(6, 6);
		bt.put(5, 5);
		bt.put(0, 0);
		bt.put(2, 2);
		bt.put(1, 1);
		assertEquals("((()0((()1())2()))4(((()5())6())7(()8())))", bt.printKeysInOrder());
	}
	
	@Test
	public void testLowestCommonAncestor(){		
		BinaryTree<Integer, Integer> bt = new BinaryTree<Integer, Integer>();
		assertEquals("()", bt.printKeysInOrder());
		
		bt.put(5, 5);	// 			_5_
		bt.put(6, 6);	//		  /     \
		bt.put(1, 1);	//		 1		 6
		bt.put(3, 3);	//	 	/ \		  \
		bt.put(0, 0);	//	   0   3	   7
		bt.put(2, 2);	//		  / \		\
		bt.put(7, 7);	//		 2	 4		 9
		bt.put(9, 9);	//  				/	
		bt.put(4, 4);	//				   8
		bt.put(8, 8);	//

		assertTrue(bt.getMax().key.equals(9));
		assertFalse(bt == null);
		assertFalse(bt.lowestCommonAncestor(2, 0) == null);
		assertTrue(bt.lowestCommonAncestor(2, 0).equals(1));
		assertTrue(bt.lowestCommonAncestor(5, 0).equals(5));
		assertTrue(bt.lowestCommonAncestor(0, 9).equals(5));
		assertTrue(bt.lowestCommonAncestor(6, 7) == 6);
		assertTrue(bt.lowestCommonAncestor(3, 3) == 3);
		assertTrue(bt.lowestCommonAncestor(3, 100) == null); //node not member of tree
		
	}

}
