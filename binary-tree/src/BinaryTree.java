import java.util.ArrayList;

public class BinaryTree<Key extends Comparable<Key>, Value> {
//ATTRIBUTES
	public Node rootNode;

//NODE CLASS
	public class Node {
		public Key key;           // sorted by key
	    public Value val;         // associated data
	    public Node left, right;  // left and right subtrees
	    public int subtreeSize;   // number of nodes in subtree
	    
	    public Node(Key k, Value v) {
	    	this.key = k;
	    	this.val = v;
	    	this.left = null;
	    	this.right = null;
	    	this.subtreeSize = 1;
	    }
	}

//METHODS
	/**
     *  Search tree for given key.
     */
    public boolean contains(Key key) {
    	return get(key) != null;
    }

    /**
     *  Recursively search tree for given key.
     */
    public Value get(Key key) { return get(rootNode, key); }

    private Value get(Node root, Key key) {
        if (root == null) return null;
        int cmp = key.compareTo(root.key);
        if      (cmp < 0) return get(root.left, key);
        else if (cmp > 0) return get(root.right, key);
        else              return root.val;
    }

	/**
     *  Insert key-value pair into tree.
     *  If key already exists, update with new value.
     */
    public void put(Key key, Value val) {
        if (val == null) { delete(key); return; }
        rootNode = put(rootNode, key, val);
    }
     
    private Node put(Node x, Key key, Value val) {
        if (x == null) return new Node(key, val);
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) x.left  = put(x.left,  key, val);
        else if (cmp > 0) x.right = put(x.right, key, val);
        else              x.val   = val;
        x.subtreeSize = 1 + size(x.left) + size(x.right);
        return x;
    }
    
    /**
     * Deletes a key from a tree (if the key is in the tree).
     * Note that this method works symmetrically from the Hibbard deletion:
     * If the node to be deleted has two child nodes, then it needs to be
     * replaced with its predecessor (not its successor) node.
     *
     * @param key the key to delete
     */
    public void delete(Key key) {
    	rootNode = delete(rootNode, key);
    }

    private Node delete(Node node, Key key) {
	    if (node == null) return null;
	    
	    int cmp = key.compareTo(node.key);
	    if (cmp < 0) 		node.left = delete(node.left, key);
	    else if (cmp > 0) 	node.right = delete(node.right, key);
	    
	    else {
		    if (node.right == null) return node.left;
		    if (node.left == null) return node.right;
		    Node temp = node;
		    node = getMax(node.left);
		    node.left = delete(temp.left, getMax(temp.left).key);
		    node.right = temp.right;
	    }
	    node.subtreeSize = size(node.left) + size(node.right) + 1;
	    return node;
    }

    /**
     * Recursively finds & returns the Node with the maximum value in this tree.
     * 
     * @return Node with largest value in tree. 
     */
    public Node getMax() {
    	return getMax(rootNode);
    }
    
    private Node getMax(Node root) {
    	if (root==null) return null;
    	if (root.right==null) return root;
    	return getMax(root.right);
    }
    
    /** return number of key-value pairs in BST */
    public int size() { return size(rootNode); }

    /** return number of key-value pairs in BST rooted at x */
    private int size(Node root) {
        if (root == null) return 0;
        else return root.subtreeSize;
    }
    
    /**
     * Print all keys of the tree in a sequence, in-order.
     * That is, for each node, the keys in the left subtree should appear before the key in the node,
     * and the keys in the right subtree should appear after the key in the node.
     * For each subtree, its keys appear within a parenthesis.
     *
     * Example 1: Empty tree -- output: "()"
     * Example 2: Tree containing only "A" -- output: "(()A())"
     * Example 3: Tree:
     *   B
     *  / \
     * A   C
     *      \
     *       D
     *
     * output: "((()A())B(()C(()D())))"
     *
     * output of example in the assignment: (((()A(()C()))E((()H(()M()))R()))S(()X()))
     *
     * @return a String with all keys in the tree, in order, parenthesised.
     */
    public String printKeysInOrder() {
      if (rootNode == null)	return "()";
      else 					return printKeysInOrder(rootNode);
    }
    
    private String printKeysInOrder(Node node) {
    	String toReturn = "";
    	toReturn = 	node.left!=null? "("+printKeysInOrder(node.left): "(()";
    	toReturn +=	node.key;
    	toReturn +=	node.right!=null? printKeysInOrder(node.right)+")": "())";
    	return toReturn;
    }
	
    /**
     * Return the Key of the lowest common ancestor (LCA) of two Nodes.
     * The LCA is: the lowest node in the tree that has both k1 and k2 as descendants, where 
     * a node can be a descendant of itself. 
     * 
     * Example: 
     *			_5_
	 *	  	  /     \
	 *	 	 1		 6
	 *		/ \		  \
	 *	   0   3	   7
	 *		  / \		\
	 *		 2	 4		 9
	 *	 				/	
	 *				   8
	 * lowestCommonAncestor(2, 0) is 1.
	 * lowestCommonAncestor(1, 5) is 5.
	 * lowestCommonAncestor(9, 9) is 9.
	 * 
     */
    public Key lowestCommonAncestor(Key k0, Key k1) {
    	if (!contains(k0) || !contains(k1))	return null;
    	
    	ArrayList<Key> path0, path1;
    	path0 = getPath(rootNode, k0);
    	path1 = getPath(rootNode, k1);
    	
    	Key ancestor0, ancestor1;
    	for(int i=0; i<path0.size() && i<path1.size(); i++){
    		ancestor0 = path0.get(i);
    		ancestor1 = path1.get(i);
    		if (ancestor0 == k0 || ancestor0 == k1)
    			return ancestor0;
    		if (!ancestor0.equals(ancestor1))
    			return path0.get(i-1);
    	}
    	return null;
    }
    
    public ArrayList<Key> getPath(Node x, Key key) {
    	ArrayList<Key> path = new ArrayList<Key>();
    	path = getPath(path, x, key);
    	return path;
    }
    
    private ArrayList<Key> getPath(ArrayList<Key> path, Node node, Key key) {
    	if(node == null)	return path;
    	
    	path.add(node.key);														// add current node to path
    	if(key.compareTo(node.key) < 0) path = getPath(path, node.left, key); 	// recurse on next node
    	else							path = getPath(path, node.right, key); 	//
    	return path;
    }
    
}
